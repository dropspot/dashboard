# README #

### Description ###

A front-end for the Dropspot geo-coding platform written in Angularjs that makes it easy to assign geo-tags to arbitrary 
web-pages. The code for the Dropspot platform is open sourced and available 
[here](https://https://bitbucket.org/dropspot/api).

### Features at glance ###

* Provides a front-end for the 
[Dropspot API](https://bitbucket.org/dropspot/dropspot-project/wiki/API%201.0%20Reference). The front-end provide means
for
    * easy geotagging of any web content by using an interactive map
    * augmenting geotags with images
    * bundling of geo-tagged content in collections

### System requirements ###

* Web server that can serve static pages
* Access to Dropspot API

### Set up ###

The project is built using [Yeoman](http://yeoman.io). For deploying the application you will need Node.js >0.10.25, 
npm (Node Packet Manager) >1.4.9 and Bower >1.3.5

Follow following steps to deploy the application

* in `config/environments/production.json` set `API_URL` to point to Dropspot API 
* in `config/environments/production.json` set `FB_APP_ID` to id of the Facebook App that will be used for 
authenticating users. It must be the same Facebook App the Dropspot API is using.
* run `npm install` from the root of the project to install Node.js dependencies
* run `bower install` from the root of the project to install Bower dependencies

On your development environment you can start the application by using Grunt server. Point to the Dropspot API in
`config/environments/production.json` and run 'grunt serve'. The application will start on port `8000`.


### Contribution guidelines ###

Feel free to create a new Pull request if you want to propose a new feature. 

### Who do I talk to? ###

* If you need development support contact us at admin@drpspt.com

### Other Dropspot Products ###

* [Dropspot API](https://https://bitbucket.org/dropspot/api)
* [Dropspot iOS App](https://https://bitbucket.org/dropspot/iosapp)
* [Dropspot Widget](https://https://bitbucket.org/dropspot/widget)
* [Dropspot Mobile Widget](https://https://bitbucket.org/dropspot/mobile-widget)

### License ###

MIT License. Copyright 2014 Dropspot GmbH. [https://bitbucket.org/dropspot/dropspot-dashboard](https://bitbucket.org/dropspot/dropspot-dashboard)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
