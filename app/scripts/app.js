/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

'use strict';

/**
 * @ngdoc overview
 * @name dropspotDashboardApp
 * @description
 * # dropspotDashboardApp
 *
 * Main module of the application.
 */
// Make sure to include the `ui.router` module as a dependency.
//var app = angular.module('dropspot');

dropspot.config(
    ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'dropspotAuthProvider', 'configuration', 'FacebookProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, dropspotAuthProvider, configuration, FacebookProvider) {
            //$locationProvider.html5Mode(true);
            dropspotAuthProvider.setUrl(configuration.API_URL + '/token/');
            FacebookProvider.init({appId: configuration.FB_APP_ID, status: true});


            //////////////////////////
            // State Configurations //
            //////////////////////////

            $urlRouterProvider.when('/', '/collections/');

            // for IE9 compatibility
            $urlRouterProvider.when('', '/collections/');


            $urlRouterProvider.otherwise("/");
            // Use $stateProvider to configure your states.

            $stateProvider

                //////////
                // Home //
                //////////
                .state("home", {
                    templateUrl: 'views/base.html',
                    url: "/",
                    controller: 'HomeCtrl'
                })

                /////////////////
                // Collections //
                /////////////////

                .state("home.collections", {
                    abstract: true,
                    templateUrl: 'views/collections_header.html',
                    controller: 'CollectionsCtrl'
                })

                .state("home.collections.all", {
                    templateUrl: 'views/collections.html',
                    url: "^/collections/",
                    controller: 'CollectionsAllCtrl'
                })

                .state("home.collections.my", {
                    templateUrl: 'views/collections.html',
                    url: "^/users/:username/collections/",
                    controller: 'CollectionsMyCtrl'
                })

                .state("home.collections.subscriptions", {
                    templateUrl: 'views/collections.html',
                    url: "^/users/:username/subscriptions/",
                    controller: 'CollectionsSubCtrl'
                })

                /////////////////
                // Collection  //
                /////////////////

                .state("home.collection", {
                    // Use a url of "/" to set a states as the "index".
                    abstract: true,
                    templateUrl: 'views/collection.html',
                    controller: 'CollectionCtrl'
                })

                .state("home.collection.map", {
                    abstract: true,
                    templateUrl: 'views/map.html',
                    controller: 'MapCtrl'
                })

                .state("home.collection.map.spots", {
                    // Use a url of "/" to set a states as the "index".
                    url: "^/collections/{collection_slug:[-_0-9a-z]+}/",
                    templateUrl: 'views/spots.html',
                    controller: 'SpotsCtrl'
                })

                .state("home.collection.map.newSpot", {
                    // Use a url of "/" to set a states as the "index".
                    url: "^/collections/{collection_slug:[-_0-9a-z]+}/spots/",
                    templateUrl: 'views/new_spot.html',
                    controller: 'NewSpotCtrl'
                })

                .state("home.collection.map.spot", {
                    // Use a url of "/" to set a states as the "index".
                    url: "^/collections/{collection_slug:[-_0-9a-z]+}/spots/{spot_slug:[-_0-9a-z]+}/",
                    templateUrl: 'views/spot.html',
                    controller: 'SpotCtrl'
                })

                .state("home.collection.map.editSpot", {
                    // Use a url of "/" to set a states as the "index".
                    url: "^/collections/{collection_slug:[-_0-9a-z]+}/spots/{spot_slug:[-_0-9a-z]+}/",
                    templateUrl: 'views/edit_spot.html',
                    controller: 'EditSpotCtrl'
                })
        }]);

