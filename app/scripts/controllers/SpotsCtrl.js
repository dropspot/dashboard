/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('SpotsCtrl', ['$scope',
        function ($scope) {
            $scope.redrawMarkers();

            $scope.showAddressPin(false);
            // waiting until all spots are loaded in a parent state
            $scope.deferredSpots.promise.then(function () {
                if(!$scope.currentSpotFlag) {
                    $scope.lookAt($scope.spots);
                }
            })
        }
    ]);