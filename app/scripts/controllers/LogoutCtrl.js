/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('LogoutCtrl', ['$rootScope', '$scope', '$modalInstance',
        function ($rootScope, $scope, $modalInstance) {
            $scope.ok = function () {
                $rootScope.$broadcast('event:logoutConfirmed');
                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);