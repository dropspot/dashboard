/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('PasswordResetCtrl', ['$scope', '$modalInstance', '$http', '$modal', 'configuration',
        function ($scope, $modalInstance, $http, $modal, configuration) {
            $scope.doneFlag = false;
            $scope.ok = function (email) {
                $scope.error = {};
                $http.post(configuration.API_URL + '/account/password/reset/', JSON.stringify({ 'email': email}))
                    .success(function () {
                        $scope.doneFlag = true;
                    })
                    .error(function (error) {
                        $scope.error = error;
                    });
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]
);