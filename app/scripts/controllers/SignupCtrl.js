/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('SignupCtrl', ['$rootScope', '$scope', '$modalInstance', 'djResource', 'dropspotAuth', 'authService', 'configuration',
        function ($rootScope, $scope, $modalInstance, djResource, dropspotAuth, authService, configuration) {
            var EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/;

            var User = djResource(configuration.API_URL + '/users/');
            $scope.user = new User();
            $scope.ok = function (user) {
                $scope.error = {};
                if (user.username !== encodeURIComponent(user.username)) {
                    $scope.error.username = 'Username may not contain special characters';
                } else if (!user.email.match(EMAIL_REGEXP)) {
                    $scope.error.email = 'Please, enter valid email address';
                } else if (user.password !== user.password2) {
                    $scope.error.password = 'Passwords don\'t match';
                } else {
                    var password = user.password;
                    user.$save(
                        function (user) {
                            dropspotAuth.login(user.username, password)
                                .then(function () {
                                    authService.loginConfirmed();
                                    $modalInstance.close();
                                },
                                function (error) {
                                    $scope.error = error;
                                });
                        },
                        function (error) {
                            $scope.error = error.data;
                        });
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $scope.login = function () {
                $modalInstance.dismiss('login');
                $rootScope.$broadcast('event:auth-loginRequired');
            };
        }]);