/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('LoginCtrl', ['$rootScope', '$scope', '$modalInstance', 'dropspotAuth', 'authService', 'Facebook', 'FBStatus',
        function ($rootScope, $scope, $modalInstance, dropspotAuth, authService, Facebook, FBStatus) {
            $scope.autocompletefix = function () {
                $("form#login input").trigger('input');
            };

            $scope.ok = function (user_creds) {
                $scope.error = {};
                dropspotAuth.login(user_creds.username, user_creds.password)
                    .then(function () {
                        authService.loginConfirmed();
                        $modalInstance.close();
                    },
                    function (error) {
                        $scope.error = error;
                    });
            };

            $scope.fbLogin = function () {
                if (FBStatus.status == 'connected') {
                    completeFbLogin(FBStatus)
                } else {
                    Facebook.login(function (response) {
                        completeFbLogin(response);
                    });
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
            $scope.signup = function () {
                $modalInstance.dismiss('signup');
                $rootScope.$broadcast('event:openSignup');
            };
            $scope.passwordReset = function () {
                $modalInstance.dismiss('signup');
                $rootScope.$broadcast('event:openPasswordReset');
            };

            var completeFbLogin = function (response) {
                dropspotAuth.loginFb(response.authResponse.accessToken)
                    .then(function () {
                        authService.loginConfirmed();
                        $modalInstance.close();
                    },
                    function (error) {
                        $scope.error = error;
                    });
            };
        }]);