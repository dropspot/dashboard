/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

/*
 Controller for home.map.collection.spot state
 */

angular.module('dropspot')
    .controller('SpotCtrl', ['$scope', '$state',
        function ($scope, $state) {

            var findSpotInSpots = function (spot_slug) {
                return $.grep($scope.spots,
                    function (e) {
                        return e.slug == spot_slug;
                    })[0];
            };

            var activateSpot = function () {
                if ($scope.spot) {
                    $scope.spot.active = true;

                    $scope.redrawMarkers();
                    $scope.lookAt([$scope.spot]);
                }
            };

            // the flag is used ot signal 'spots' state not to trigger lookAt function (see issue #187
            // https://bitbucket.org/dropspot/dropspot-project/issue/187/the-user-has-to-wait-until-all-spots-are)
            $scope.$parent.currentSpotFlag = true;
            $scope.showAddressPin(false);
            $scope.$parent.spot = null;

            $scope.showAddressPin(false);

            // searching for the selected spot among $scope.spots
            if ($scope.spots) {
                $scope.$parent.spot = findSpotInSpots($scope.$stateParams.spot_slug);
            }
            if ($scope.spot) {
                activateSpot();
            } else {

                // in case the spot is accessed directly via url, the spots list is not filled yet,
                // so an extra call for the spot is made.
                $scope.$parent.spot = $scope.Spot.get({spot_slug: $scope.$stateParams.spot_slug},
                    function () {
                        // waiting until all spots are loaded in a parent state and mark the current spot as active
                        $scope.deferredSpots.promise.then(function () {

                            // searching for the selected spot among $scope.spots
                            $scope.$parent.spot = findSpotInSpots($scope.$stateParams.spot_slug);

                            $scope.spot.active = true;
                            $scope.redrawMarkers();
                            activateSpot();
                        });
                    });
            }

            var get_options = function () {
                $scope.Spot.options({spot_slug: $scope.$stateParams.spot_slug}, function (data) {
                    $scope.can_create_spot = 'actions' in data && 'PUT' in data['actions'];
                    $scope.can_edit = 'actions' in data && 'PUT' in data['actions'];
                    if ($scope.can_edit) {
                        $scope.fields = _.keys(data['actions']['PUT']);
                    }
                });
            };
            get_options();
            $scope.$on('event:auth-loginConfirmed', get_options);
            $scope.$on('event:logoutConfirmed', get_options);

            // cleaning up when leaving the state
            $scope.$on('$destroy', function () {
                $scope.spot.active = false;
                $scope.spot.draggable = false;
                $scope.$parent.currentSpotFlag = false;
            });

            $scope.cancel = function () {
                $scope.redrawMarkers();
                $state.go('home.collection.map.spots', {collection_slug: $scope.collection.slug});
            };

            $scope.collection_tags = [];

            $scope.tagChanged = function () {
                $scope.spot.collection_slugs = $scope.collection_tags.join()
            };
        }
    ]);