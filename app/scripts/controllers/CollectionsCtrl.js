/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('CollectionsCtrl', ['$scope', '$state', 'Collection',
        function ($scope, $state, Collection) {
            $scope.editable_collection = new Collection();
            $scope.saveCollection = function () {
                $scope.editable_collection.createCollection($scope.editable_collection.name, $scope.editable_collection.description).then(function (data) {
                    $state.go('home.collection.map.spots', {collection_slug: data['data']['slug']});
                    $scope.editable_collection = null;
                }, function (error) {
                    $scope.collection_error = error.data
                });
            }
        }
    ]);