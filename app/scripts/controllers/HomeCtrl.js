/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('HomeCtrl', ['$rootScope', '$scope', '$timeout', '$state', '$http',
        'angulargmContainer', '$modal', 'dropspotAuth', 'authService', 'configuration', 'Facebook',
        function ($rootScope, $scope, $timeout, $state, $http, angulargmContainer, $modal, dropspotAuth, authService, configuration, Facebook) {
            navigator.geolocation.getCurrentPosition(function (position) {
                $timeout(function () {
                    $scope.center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                });
            });

            // making the service available to all states
            $scope.dropspotAuth = dropspotAuth;

            if (dropspotAuth.logged()) {
                $http.defaults.headers.common['Authorization'] = 'Token ' + dropspotAuth.user().token;
                $http.get(configuration.API_URL + '/profile/', {ignoreAuthModule: true})
                    .success(function (data) { })
                    .error(function () {
                    dropspotAuth.logout();
                });
            } else {
                // Watch for when Facebook is ready and loaded
                $scope.$watch(function() {
                    return Facebook.isReady(); // This is for convenience, to notify if Facebook is loaded and ready to go.
                }, function(newVal) {
                    $rootScope.facebookReady = newVal; // You might want to use this to disable/show/hide buttons and else
                });
            }

            $scope.openLogin = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'views/login.html',
                    controller: 'LoginCtrl',
                    resolve: {
                        // is required to avoid colling Facebook.login from a callback of Facebook.getLoginStatus,
                        // which causes Facebook popup to be blocked otherwise
                        FBStatus: ['$q', 'Facebook', function($q, Facebook) {
                            var defer = $q.defer();

                            Facebook.getLoginStatus(function(result) {
                                defer.resolve(result);
                            });

                            return defer.promise;
                        }]
                    }
                });

                modalInstance.result.then(function () {
                }, function () {
                    authService.loginCancelled();
                });
            };

            $scope.openLogout = function () {
                $modal.open({
                    templateUrl: 'views/logout.html',
                    size: "sm",
                    controller: 'LogoutCtrl'
                });
            };

            $scope.openSignup = function () {
                $modal.open({
                    templateUrl: 'views/signup.html',
                    controller: 'SignupCtrl'
                });
            };

            $scope.openPasswordReset = function () {
                $modal.open({
                    templateUrl: 'views/password_reset.html',
                    controller: 'PasswordResetCtrl'
                });
            };

            $scope.openPasswordChange = function () {
                $modal.open({
                    templateUrl: 'views/password_change.html',
                    controller: 'PasswordChangeCtrl'
                });
            };

            $scope.openProfile = function () {
                $modal.open({
                    templateUrl: 'views/profile.html',
                    controller: 'ProfileCtrl'
                });
            };

            $scope.$on('event:openSignup', function () {
                $scope.openSignup();
            });

            $scope.$on('event:openPasswordChange', function () {
                $scope.openPasswordReset();
            });
            $scope.$on('event:auth-loginRequired', function () {
                $scope.openLogin();
            });
            $scope.$on('event:auth-loginConfirmed', function () {
                $http.defaults.headers.common['Authorization'] = 'Token ' + dropspotAuth.user().token;
                $http.get(configuration.API_URL + '/profile/',  {ignoreAuthModule: true}).success(function (data) {
                    $scope.auth_user = data;
                });
            });
            $scope.$on('event:logoutConfirmed', function () {
                delete $http.defaults.headers.common['Authorization'];
                dropspotAuth.logout();
            });
        }]);