/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('ProfileCtrl', ['$rootScope', '$scope', '$upload', '$modalInstance', 'djResource', 'configuration',
        function ($rootScope, $scope, $upload, $modalInstance, djResource, configuration) {
            $scope.message = '';
            var EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$/;

            var User = djResource(configuration.API_URL + '/profile/', null, {
                'update': {
                    method: 'PUT'
                },
                'options': {method: 'OPTIONS'}
            });

            var Avatars = djResource(configuration.API_URL + '/users/:username/avatars/', {username: '@username'}, {
                'save': {
                    method: 'POST'
                },
                'options': {method: 'OPTIONS'},
                'delete': {method: 'DELETE'}
            });
            var Avatar = djResource(configuration.API_URL + '/users/:username/avatars/:id/', {
                username: '@username',
                id: '@id'}, {
                'update': {
                    method: 'PATCH'
                },
                'options': {method: 'OPTIONS'},
                'delete': {method: 'DELETE'}
            });

            $scope.user = new User();
            $scope.avatars = new Avatars();
            $scope.user.$get(function(data) {
                $scope.avatars = Avatars.query({username: $scope.user.username});
            });

            // handle new avatar pictures
            $scope.newImage = new Avatars({username: $scope.user.username});
            $scope.newImage.primary = true;
            $scope.onAvatarFileSelect = function($files) {
                var $file = $files[0];
                $scope.upload = $upload.upload({
                    url: configuration.API_URL + '/users/' + $scope.user.username + '/avatars/',
                    method: 'POST',
                    data: $scope.newImage,
                    file: $file,
                    fileFormDataName: 'avatar'
                }).success(function (data) {
                    $scope.avatars = Avatars.query({username: $scope.user.username});
                },
                function (error) {
                    $scope.uploaderror = error.data;
                 });
            };

            $scope.setPrimary = function(avatar){
                angular.forEach($scope.avatars, function(av) {
                    av.primary = false;
                });
                avatar.primary = true;
            }

            $scope.removeAvatar = function(avatar){
                avatar.__proto__ = Avatar.prototype;

                avatar.$delete({username: $scope.user.username, id:avatar.id}, function(){
                    $scope.avatars = Avatars.query({username: $scope.user.username});
                },
                function (error) {
                    $scope.uploaderror = error.data;
                });
            }


            $scope.ok = function (user) {
                $scope.error = {};
                if (user.username !== encodeURIComponent(user.username)) {
                    $scope.error.username = 'Username may not contain special characters';
                } else if (!user.email.match(EMAIL_REGEXP)) {
                    $scope.error.email = 'Please, enter valid email address';
                } else {
                    user.$update(
                        function () {
                            angular.forEach($scope.avatars, function (avatar) {
                                // type conversion to change resource (primary)
                                avatar.__proto__ = Avatar.prototype;
                                delete avatar.url;
                                delete avatar.avatar;
                                avatar.$update({username: $scope.user.username});
                            });
                            $scope.message = "Your profile data was successfuly updated"
                        },
                        function (error) {
                            $scope.error = error.data;
                        });



                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);