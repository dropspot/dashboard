/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('CollectionCtrl', ['$scope', '$state', '$timeout', '$upload', '$q', '$modal', 'djResource','$filter', 'configuration',
        function ($scope, $state, $timeout, $upload, $q, $modal, djResource, $filter, configuration) {
            // creating a deferred object, that is resolved when all the spots are loaded
            $scope.deferredSpots = $q.defer();

            $scope.Collection = djResource(configuration.API_URL + '/collections/:collection_slug/', {collection_slug: '@slug'}, {
                'update': {
                    method: 'PUT'
                },
                'options': {method: 'OPTIONS'},
                'delete': {method: 'DELETE'}
            });

            $scope.Spot = djResource(configuration.API_URL + '/spots/:spot_slug/', {spot_slug: '@slug'}, {
                'update': {
                    method: 'PUT'
                },
                'options': {method: 'OPTIONS'},
                'delete': {method: 'DELETE'}
            });

            $scope.Link = djResource(configuration.API_URL + '/collections/:collection_slug/spots/:spot_slug/links/:link_id/',
                {
                    link_id: '@id'
                },
                {
                    'update': { method: 'PUT'}
                });

            $scope.SpotLinks = djResource(configuration.API_URL + '/collections/:collection_slug/spots/:spot_slug/links/');

            $scope.collection = $scope.Collection.get({collection_slug: $scope.$stateParams.collection_slug},
                function (data) {
                    $scope.collection = data;

                    // after the collection is loaded, load the collection's spots
                    var CollectionSpots = djResource(configuration.API_URL + '/collections/:collection_slug/spots/');
                    $scope.spots = CollectionSpots.query({collection_slug: $scope.$stateParams.collection_slug},
                        function () {
                            $scope.deferredSpots.resolve();
                        });
                });
            var get_options = function () {
                $scope.Collection.options({collection_slug: $scope.$stateParams.collection_slug}, function (data) {
                    $scope.can_edit_collection = 'actions' in data && 'PUT' in data['actions'];
                    // This is not 100% accurate as an admin can edit a collection
                    // but cannot add spots to it. In practice it works.
                    $scope.can_create_spot = $scope.can_edit_collection;
                });
            };
            get_options();

            $scope.$on('event:auth-loginConfirmed', get_options);
            $scope.$on('event:logoutConfirmed', get_options);

            $scope.saveCollection = function () {
                var postUpdate = function (collection) {
                    $scope.collection = collection;
                    $scope.editable_collection = null;
                    $timeout(function () {
                        $state.go($scope.$state.current, $scope.$stateParams, { reload: true, inherit: false, notify: true });
                    })
                };
                var collectionError = function (error) {
                    $scope.collection_error = error.data;

                    // restore the cover photo, otherwise no photo is shown
                    $scope.editable_collection.photo = photo;
                };
                var photo = $scope.editable_collection.photo;
                delete $scope.editable_collection.photo;
                $scope.editable_collection.$update(postUpdate, collectionError);
            };

            /**
            * If the protocol of a link is not specified, prepends the link with 'http'
            *
            * @param link
            * @returns {*}
            */
            $scope.linkToURLFiled = function(link) {
                if (link && !/^https?:\/\//i.test(link)) {
                    link = 'http://' + link;
                }
                return link;
            };

            $scope.deleteCollection = function () {
                $scope.editable_collection.$delete(function () {
                    $state.go('home.collections.all');
                });
            };

            $scope.cancelEdit = function () {
                $scope.editable_collection = null;
            };

            $scope.editCollection = function () {
                $scope.collection_error = null;
                $scope.editable_collection = _.extend(new $scope.Collection(), _.pick($scope.collection, ['name', 'slug', 'url', 'photo', 'description', 'published']));
            };

            $scope.onCollectionFileSelect = function ($files) {
                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                    var $file = $files[i];
                    $scope.upload = $upload.upload({
                        url: $scope.editable_collection.url, //upload.php script, node.js route, or servlet url
                        method: 'PUT',
                        data: $scope.editable_collection,
                        file: $file,
                        //(optional) set 'Content-Desposition' formData name for file
                        fileFormDataName: 'photo',
                        progress: function (evt) {
                            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                        }
                    }).success(function (data) {
                        // file is uploaded successfully
                        $scope.progress = null;

                        $scope.editable_collection.photo = data.photo;

                        // since we are using PUT (to get around BILD network issues) when a file is uploaded
                        // the current changes to the collection are saved - a better solution would be nice
                        $scope.collection = $scope.editable_collection;
                    });
                    //.error(...).then(...);
                }
            };

            $scope.expirationCountdown = function(spot) {
                if(spot.expiration_date){
                    var current = new Date();
                    var spot_date = new Date(spot.expiration_date);

                    return Math.round((spot_date - current) / 1000 / 60 / 60 / 24);
                }
            };

            $scope.minDate = new Date();
            $scope.calendar = false;
            $scope.dateTimeToDate = function(date) {
                myDate = new Date(date);
                return $filter('date')(new Date(date),'yyyy-MM-dd');
            };

            $scope.noExpireDate = function(spot){
                $scope.calendar = false;
                spot.expiration_date = null
            };

            $scope.hasExpireDate = function(spot){
                $scope.calendar = true;
                if(spot.expiration_date == null){
                    current = new Date();
                    expire = new Date(current.getTime() + 28 * 24 * 60 * 60 * 1000);
                    spot.expiration_date = $filter('date')(new Date(expire),'yyyy-MM-dd');
                }
            };

            /**
             * Opens a Modal and shows the widget for the current Collection
             * After the Modal is closed the ZeroClipboard Instance get destroyed
             * So every Modal has his own Clipper.
             *
             * copy_check handles the css class to show and hide clipboard copy notification
             */
            $scope.get_widget = function () {
                $modal.open({
                    templateUrl: 'views/modal_widget.html',
                    size: 'lg',
                    controller: function ($scope, $modalInstance, $sce) {
                        // Widget Code Text

                        var collection_slug = $scope.$parent.$stateParams.collection_slug;
                        $scope.widget_code = '<div class="dropspot-collection" data-slug="' +
                            collection_slug +
                            '" data-style="map"></div>\n\n' +
                            '<!-- Place this tag after the last widget tag. -->\n' +
                            '<script src="' + widgetPath + '" type="text/javascript"></script>';

                        $scope.widget_div = $sce.trustAsHtml($scope.widget_code);

                        // Handle CopyToClipboard
                        $scope.copy_check = false;
                        $scope.ok = function () {
                            $scope.copy_check = false;
                            ZeroClipboard.destroy();
                            $modalInstance.close();
                        };

                        // Can handle ONLY the ClipboardText
                        $scope.getTextToCopy = function () {
                            return $scope.widget_code;
                        };
                        // Handle css classes for clipboard confirmation
                        $scope.getCopyStatus = function () {
                            if ($scope.copy_check === false)
                                $scope.copy_check = true;
                        };
                    }
                });
            }
        }
    ]);
