/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('NewSpotCtrl', ['$scope', '$state', '$timeout', 'configuration',
        function ($scope, $state, $timeout, configuration) {
            $scope.redrawMarkers();

            // used in $destroy event handler - if not set, $scope.spot is reset to originalSpot
            var savingFlag = false;

            $scope.spot_error = null;
            $scope.spot = new $scope.Spot();
            $scope.collection_tags = [];
            $scope.spot.latitude = $scope.center.lat();
            $scope.spot.longitude = $scope.center.lng();

            // the flag is used ot signal 'spots' state not to trigger lookAt function (see issue #187
            // https://bitbucket.org/dropspot/dropspot-project/issue/187/the-user-has-to-wait-until-all-spots-are)
            $scope.$parent.currentSpotFlag = true;
            $scope.spot.active = true;
            $scope.spot.draggable = true;
            $scope.showAddressPin(true);
            $scope.$parent.spots.unshift($scope.spot);
            $scope.$parent.spot = $scope.spot;

            var g = new google.maps.Geocoder();
            g.geocode({'latLng': $scope.center}, function (results, status) {
                $timeout(function () {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $scope.address_suggestion = results[0].formatted_address;
                            $scope.spot.address = $scope.address_suggestion;
                        }
                    }
                });
            });

            $scope.redrawMarkers();
            $scope.address_suggestion = null;
            if ($scope.collection) {
                $scope.collection_tags = [$scope.collection.slug];
                $scope.spot.collection_slugs = $scope.collection.slug;
            }

            if ($scope.spot.expiration_date) {
                $scope.calendar = true;
                $scope.spot.expiration_date = $scope.$parent.dateTimeToDate($scope.spot.expiration_date);
            }

            $scope.newSpotLinks = [];
            $scope.addSpotLink = function () {
                if ($scope.addLink && $scope.addLink !== '') {
                    var newLink = new $scope.Link();
                    $scope.addLink = $scope.linkToURLFiled($scope.addLink);
                    newLink.link = $scope.addLink;
                    //newLink.spot = $scope.spot;
                    $scope.newSpotLinks.push(newLink);
                    $scope.addLink = null;
                }
            };

            $scope.removeTempLink = function (link) {
                for (var i = 0; i < $scope.newSpotLinks.length; i++) {
                    if ($scope.newSpotLinks[i].id === link.id) {
                        $scope.newSpotLinks.splice(i, 1);
                    }
                }
            };

            // cleaning up when leaving the state
            $scope.$on('$destroy', function () {
                // nothing to clean if the spot was deleted
                if ($scope.spot) {
                    $scope.spot.active = false;
                    $scope.spot.draggable = false;
                    $scope.$parent.currentSpotFlag = false;
                }

                //if the spot is not saved, reset the changes
                if (!savingFlag) {
                    $scope.cancelSpot();
                }
            });

            $scope.saveNewSpot = function () {

                var postSave = function (spot) {
                    // when spot saved save all additional spot links
                    if ($scope.addLink !== undefined) {
                        $scope.addSpotLink();
                    }

                    for (var i = 0; i < $scope.newSpotLinks.length; i++) {
                        $scope.spot.content_links.links.push($scope.newSpotLinks[i].link);
                        // anonymous function is used to avoid
                        // 'Mutable variable is accessible from closure' warning
                        (function () {
                            var tmp_i = i;
                            if ($scope.newSpotLinks[tmp_i]) {
                                $scope.newSpotLinks[tmp_i].$save({
                                    collection_slug: $scope.$stateParams.collection_slug,
                                    spot_slug: $scope.spot.slug
                                })
                            }
                        })()
                    }

                    $scope.redrawMarkers();

                    // update url of the spot, otherwise it will be set to /spots/slug
                    spot.url = $scope.collection.url + 'spots/' + spot.slug + '/';

                    if (_.indexOf(spot.collection_slugs.split(","), $scope.collection.slug) != -1) {
                        // You are removing the current collection from the slugs
                    }
                    $scope.collection.spot_count++;
                    $state.go('home.collection.map.spot', {collection_slug: $scope.collection.slug, spot_slug: spot.slug});
                };
                var spotError = function (error) {
                    $scope.spot_error = error.data;

                    $scope.cancelSpot()
                };

                $scope.spot.link = $scope.linkToURLFiled($scope.spot.link);

                savingFlag = true;
                if ($scope.spot.expiration_date) {
                    $scope.spot.expiration_date = new Date($scope.spot.expiration_date)
                } else {
                    $scope.spot.expiration_date = null;
                }
                $scope.spot.$save(postSave, spotError);
            };

            $scope.cancel = function () {
                $state.go('home.collection.map.spots', {collection_slug: $scope.collection.slug});
            };

            $scope.cancelSpot = function () {
                $scope.spot = null;
                $scope.$parent.$parent.spots.pop();
            }

        }
    ]);
