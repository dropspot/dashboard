/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('CollectionsSubCtrl',['$scope', 'Subscription', 'configuration',
        function ($scope, Subscription, configuration) {
            // setting path for Collection.prototype.nextPage
            $scope.path = configuration.API_URL + '/users/' + $scope.$stateParams.username + '/subscriptions/';

            $scope.collections = new Subscription();
        }
    ]);