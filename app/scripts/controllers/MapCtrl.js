/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

var app = angular.module('dropspot');

app.controller('MapCtrl', ['$scope', '$timeout', '$state', 'djResource', 'angulargmContainer', '$location',
    function ($scope, $timeout, $state, djResource, angulargmContainer,
              $location) {
        $scope.spot = null;

        $scope.options = {
            map: {
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                scaleControl: true,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                zoom: 3,
                center: new google.maps.LatLng(0, 0)
            },
            autocomplete: {
                watchEnter: true
            }
        };

        $scope.autocomplete = {detail: ''};

        $scope.$watch('autocomplete.detail', function () {
            if ($scope.autocomplete.detail) {
                if ($scope.autocomplete.detail.geometry.viewport) {
                    var ne = $scope.autocomplete.detail.geometry.viewport.getNorthEast();
                    var sw = $scope.autocomplete.detail.geometry.viewport.getSouthWest();
                    $scope.lookAt([
                        {latitude: ne.lat(), longitude: ne.lng()},
                        {latitude: sw.lat(), longitude: sw.lng()}
                    ]);
                } else {
                    $scope.zoom = 16;
                    var l = $scope.autocomplete.detail.geometry.location;
                    $scope.lookAt([
                        {latitude: l.lat(), longitude: l.lng()}
                    ]);
                }
            }
        });

        $scope.markerOptions = function (spot) {
            options = { title: spot.name};
            if (!spot.active) {
                options['icon'] = '/images/spot.png';
            } else {
                delete options['icon'];
                options['clickable'] = false;
            }
            if (spot.draggable) {
                options['draggable'] = true;
                options['animation'] = google.maps.Animation.BOUNCE;
            }
            return options;
        };

        $scope.suggestAddress = function (object, marker) {
            var position = marker.getPosition();
            $scope.spot.latitude = position.lat();
            $scope.spot.longitude = position.lng();
            var g = new google.maps.Geocoder();
            g.geocode({'latLng': position}, function (results, status) {
                $timeout(function () {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $scope.address_suggestion = results[0].formatted_address;
                            $scope.spot.address = $scope.address_suggestion;
                        }
                    }
                });
            });
        };

        $scope.markerClick = function (url) {
            var l = document.createElement("a");
            l.href = url;
            $location.path(l.pathname);
        };

        $scope.lookAt = function (spots) {
            if (spots && spots.length > 0) {
                if (spots.length == 1) {
                    $scope.center = new google.maps.LatLng(spots[0].latitude, spots[0].longitude);
                    $scope.zoom = 14;
                } else {
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < spots.length; i++) {
                        var p = new google.maps.LatLng(spots[i].latitude, spots[i].longitude);
                        bounds.extend(p);
                    }
                    $scope.bounds = bounds;
                }
            }
        };

        $scope.mapSpots = function (spots) {
            $scope.last_spots = $scope.spots || spots;
            $scope.spots = spots;
        };

        $scope.redrawMarkers = function () {
            $scope.$broadcast('gmMarkersRedraw');
        };

        $scope.newEditPosition = function () {
            $scope.spot.latitude = $scope.center.lat();
            $scope.spot.longitude = $scope.center.lng();

            var g = new google.maps.Geocoder();
            g.geocode({'latLng': $scope.center}, function (results, status) {
                $timeout(function () {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $scope.address_suggestion = results[0].formatted_address;
                            $scope.spot.address = $scope.address_suggestion;
                        }
                    }
                });
            });
            $scope.redrawMarkers();
            $scope.address_suggestion = null;
            if ($scope.collection) {
                $scope.collection_tags = [$scope.collection.slug];
                $scope.spot.collection_slugs = $scope.collection.slug;
            }
        };

        $scope.showAddressPin = function (flag) {
            $scope.addressPin = flag;
        }

    }]);