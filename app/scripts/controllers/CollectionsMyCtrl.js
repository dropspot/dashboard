/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('CollectionsMyCtrl', ['$scope', 'Collection', 'configuration',
        function ($scope, Collection, configuration) {
            // setting path for Collection.prototype.nextPage
            $scope.path = configuration.API_URL + '/users/' + $scope.$stateParams.username + '/collections/';

            $scope.collections = new Collection();
        }
    ]);