/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

/*
 Controller for home.map.collection.spot state
 */

angular.module('dropspot')
    .controller('EditSpotCtrl', ['$scope', '$state', '$upload', '$filter', '$q', 'configuration',
        function ($scope, $state, $upload, $filter, $q, configuration) {
            // creating a copy of the selected spot to have the possibility for
            // cancelling the changes in case they are not saved
            var originalSpot = angular.copy($scope.spot);

            // used in $destroy event handler - if not set, $scope.spot is reset to originalSpot
            var savingFlag = false;

            var deferredSavedLinks = $q.defer();
            var deferredUpdatedLinks = $q.defer();
            var deferredDeletedLinks = $q.defer();

            // the flag is used ot signal 'spots' state not to trigger lookAt function (see issue #187
            // https://bitbucket.org/dropspot/dropspot-project/issue/187/the-user-has-to-wait-until-all-spots-are)
            $scope.$parent.currentSpotFlag = true;
            $scope.spot.active = true;
            $scope.spot.draggable = true;
            $scope.showAddressPin(true);
            $scope.spot_error = null;
            $scope.address_suggestion = null;

            // 'casting' spot to Spot type,
            // otherwise $update is not available and $delete is working incorrectly
            $scope.spot.__proto__ = $scope.Spot.prototype;

            $scope.spotLinks = $scope.SpotLinks.query({
                collection_slug: $scope.$stateParams.collection_slug,
                spot_slug: $scope.spot.slug
            }, function () {
            });

            if ($scope.spot.collection_slugs != '') {
                $scope.collection_tags = $scope.spot.collection_slugs.split(',');
            } else {
                $scope.collection_tags = [];
            }
            $scope.redrawMarkers();

            if ($scope.spot.expiration_date) {
                $scope.calendar = true;
                $scope.spot.expiration_date = $scope.$parent.dateTimeToDate($scope.spot.expiration_date);
            }

            $scope.spotLinksDelete = [];
            $scope.newSpotLinks = [];
            $scope.addSpotLink = function () {
                if ($scope.addLink !== undefined && $scope.addLink !== '') {
                    var newLink = new $scope.Link();
                    $scope.addLink = $scope.linkToURLFiled($scope.addLink);
                    newLink.link = $scope.addLink;
                    $scope.newSpotLinks.push(newLink);
                    $scope.addLink = null;
                }
            };

            $scope.deleteLink = function (link) {
                link.__proto__ = $scope.Link.prototype;
                $scope.spotLinksDelete.push(link);
                for (var i = 0; i < $scope.spotLinks.length; i++) {
                    if ($scope.spotLinks[i].id === link.id) {
                        $scope.spotLinks.splice(i, 1);
                    }
                }
            };

            $scope.removeTempLink = function (link) {
                for (var i = 0; i < $scope.newSpotLinks.length; i++) {
                    if ($scope.newSpotLinks[i].id === link.id) {
                        $scope.newSpotLinks.splice(i, 1);
                    }
                }
            };

            var get_options = function () {
                $scope.Spot.options({
                    spot_slug: $scope.$stateParams.spot_slug
                }, function (data) {
                    $scope.can_create_spot = 'actions' in data && 'PUT' in data['actions'];
                    $scope.can_edit = 'actions' in data && 'PUT' in data['actions'];
                    if ($scope.can_edit) {
                        $scope.fields = _.keys(data['actions']['PUT']);
                    }
                });
            };
            get_options();
            $scope.$on('event:auth-loginConfirmed', get_options);
            $scope.$on('event:logoutConfirmed', get_options);

            // cleaning up when leaving the state
            $scope.$on('$destroy', function () {
                $scope.$parent.currentSpotFlag = false;

                if ($scope.spot) {
                    $scope.spot.active = false;
                    $scope.spot.draggable = false;
                }

                //if the spot is not saved, reset the changes
                if (!savingFlag) {
                    $scope.cancelSpot();
                }
            });


            $scope.onSpotFileSelect = function ($files) {
                //$files: an array of files selected, each file has name, size, and type.
                for (var i = 0; i < $files.length; i++) {
                    // Dirty hack to use PUT instead of PATCH when uploading images
                    // (due to BILD network blocking our PATCH requests
                    var tmpSpot = JSON.parse(JSON.stringify($scope.spot));
                    if (tmpSpot.expiration_date) {
                        tmpSpot.expiration_date = (new Date(tmpSpot.expiration_date)).toISOString();
                    } else {
                        delete tmpSpot.expiration_date;
                    }
                    var $file = $files[i];
                    $scope.upload = $upload.upload({
                        url: $scope.spot.url, //upload.php script, node.js route, or servlet url
                        method: 'PUT',
                        data: tmpSpot,
                        file: $file,
                        //(optional) set 'Content-Disposition' formData name for file
                        fileFormDataName: 'photo',
                        progress: function (evt) {
                            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                        }
                    }).success(function (data) {
                        $scope.progress = null;

                        // file is uploaded successfully
                        $scope.spot.photo = data.photo;

                        // since we are using PUT (to get around BILD network issues) when a file is uploaded
                        // the current changes to the spot are saved - a better solution would be nice
                        savingFlag = true;
                    });
                    //.error(...).then(...);
                }
            };

            $scope.cancel = function () {
                $state.go('home.collection.map.spot', {
                    collection_slug: $scope.collection.slug,
                    spot_slug: $scope.spot.slug
                });
            };

            $scope.cancelSpot = function () {
                $scope.spot.name = originalSpot.name;
                $scope.spot.latitude = originalSpot.latitude;
                $scope.spot.longitude = originalSpot.longitude;
                $scope.spot.address = originalSpot.address;
                $scope.spotLinksDelete = [];
                $scope.newSpotLinks = [];
            };

            $scope.deleteSpot = function () {
                if ($scope.spot.slug) {
                    savingFlag = true;

                    $scope.spot.$delete(function () {
                        // remove deleted spot from $scope.spots
                        for (var i = 0; i < $scope.$parent.spots.length; i++) {
                            if ($scope.$parent.spots[i].slug === $scope.spot.slug) {
                                $scope.$parent.spots.splice(i, 1);
                            }
                        }

                        $scope.spot = null;
                        $scope.redrawMarkers();
                        $scope.collection.spot_count--;
                        $state.go('home.collection.map.spots', {
                            collection_slug: $scope.collection.slug
                        })
                    }, function (error) {
                        $scope.error = error;
                    });
                }
                $scope.redrawMarkers();
            };

            $scope.saveSpot = function () {
                var postUpdate = function (spot) {
                    // update url of the spot, otherwise it will be set to /spots/slug
                    spot.url = $scope.collection.url + 'spots/' + spot.slug + '/';

                    $state.go('home.collection.map.spot', {
                        collection_slug: $scope.$stateParams.collection_slug,
                        spot_slug: $scope.spot.slug
                    });
                };

                var spotError = function (error) {
                    $scope.spot_error = error.data;

                    // restore the cover photo, otherwise no photo is shown
                    $scope.spot.photo = photo;

                    $scope.cancelSpot();
                };

                var photo = $scope.spot.photo;
                delete $scope.spot.photo;
                $scope.spot.link = $scope.linkToURLFiled($scope.spot.link);

                if ($scope.spot.expiration_date) {
                    $scope.spot.expiration_date = new Date($scope.spot.expiration_date)
                } else {
                    $scope.spot.expiration_date = null;
                }

                savingFlag = true;

                // Save all new links
                if ($scope.addLink) {
                    $scope.addSpotLink();
                }
                if ($scope.newSpotLinks.length == 0) {
                    deferredSavedLinks.resolve();
                } else {
                    for (var i = 0; i < $scope.newSpotLinks.length; i++) {
                        // anonymous function is used to avoid
                        // 'Mutable variable is accessible from closure' warning
                        (function () {
                            var tmp_i = i;
                            if ($scope.newSpotLinks[tmp_i]) {
                                $scope.newSpotLinks[tmp_i].$save({
                                    collection_slug: $scope.$stateParams.collection_slug,
                                    spot_slug: $scope.spot.slug
                                }, function () {
                                    if (tmp_i == $scope.newSpotLinks.length - 1) {
                                        deferredSavedLinks.resolve();
                                    }
                                }, spotError)
                            }
                        })()
                    }
                }

                // Save all existing links
                if ($scope.spotLinks.length == 0) {
                    deferredUpdatedLinks.resolve();
                } else {
                    for (var j = 0; j < $scope.spotLinks.length; j++) {
                        $scope.spotLinks[j].link = $scope.linkToURLFiled($scope.spotLinks[j].link);
                        (function () {
                            var tmp_j = j;
                            $scope.spotLinks[tmp_j].__proto__ = $scope.Link.prototype;
                            $scope.spotLinks[tmp_j].$update({
                                collection_slug: $scope.$stateParams.collection_slug,
                                spot_slug: $scope.spot.slug,
                                link_id: $scope.spotLinks[tmp_j].id
                            }, function () {
                                if (tmp_j == $scope.spotLinks.length - 1) {
                                    deferredUpdatedLinks.resolve();
                                }
                            }, spotError)
                        })()
                    }
                }

                // Delete all removed links
                if ($scope.spotLinksDelete.length == 0) {
                    deferredDeletedLinks.resolve();
                } else {
                    for (var k = 0; k < $scope.spotLinksDelete.length; k++) {
                        (function () {
                            var tmp_k = k;
                            $scope.spotLinksDelete[tmp_k].$delete({
                                collection_slug: $scope.$stateParams.collection_slug,
                                spot_slug: $scope.spot.slug,
                                link_id: $scope.spotLinksDelete[tmp_k].id
                            }, function () {
                                if (tmp_k == $scope.spotLinksDelete.length - 1) {
                                    deferredDeletedLinks.resolve();
                                }
                            }, spotError)
                        })()
                    }
                }

                // waiting until all links are updated
                $q.all([
                    deferredSavedLinks.promise,
                    deferredUpdatedLinks.promise,
                    deferredDeletedLinks.promise
                ]).then(function () {
                    $scope.spot.$update(postUpdate, spotError)
                });

            };
            $scope.collection_tags = [];

            $scope.tagChanged = function () {
                $scope.spot.collection_slugs = $scope.collection_tags.join()
            };

            $scope.loadItems = function ($query) {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: configuration.API_URL + '/profile/.json'
                }).
                    success(function (data) {
                        var message = _.pluck(data['collections'], 'slug');
                        message = $filter('filter')(message, $query, false);

                        deferred.resolve(message);
                        //deferred.resolve(['aaa' + $query, 'bbb' + $query]);
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });

                return deferred.promise;
            };
        }
    ]);
