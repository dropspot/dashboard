/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('PasswordChangeCtrl', ['$scope', '$modalInstance', '$http', '$modal', 'configuration',
        function ($scope, $modalInstance, $http, $modal, configuration) {
            $scope.doneFlag = false;
            $scope.ok = function (passwords) {
                $scope.error = {};
                if (passwords.newpassword !== passwords.newpassword2) {
                    $scope.error.newpassword = 'Passwords don\'t match';
                } else {
                    $http.post(configuration.API_URL + '/account/password/change/',
                        JSON.stringify(
                            {
                                'newpassword': passwords.newpassword,
                                'oldpassword': passwords.oldpassword
                            }))
                        .success(function () {
                            $scope.doneFlag = true;
                        })
                        .error(function (error) {
                            $scope.error = error;
                        });
                }
            };
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]
);