/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

angular.module('dropspot')
    .controller('CollectionsAllCtrl', ['$scope', 'Collection', 'configuration',
        function ($scope, Collection, configuration) {
            // setting path for Collection.prototype.nextPage
            $scope.path = configuration.API_URL + '/collections/';

            $scope.collections = new Collection();
        }
    ]);