/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

'use strict';
angular.module('dropspot.tokenauth', [
    'webStorageModule',
    'ngCookies'
]).provider('dropspotAuth', function () {
    this.url = '/auth';
    this.setUrl = function (url) {
        this.url = url;
    };
    this.$get = [
        'webStorage',
        '$http',
        '$q',
        '$cookies',
        function (webStorage, $http, $q, $cookies) {
            var that = this;
            var csrftoken = $cookies.csrftoken, url = that.url;
            return {
                logged: function () {
                    return !!webStorage.local.get('token');
                },
                login: function (username, password) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: url,
                        data: {
                            username: username,
                            password: password
                        },
                        headers: { 'X-CSRFToken': csrftoken }
                    }).success(function (data) {
                        webStorage.local.add('token', data.token);
                        webStorage.local.add('username', username);
                        deferred.resolve({
                            success: 'login',
                            username: username
                        });
                    }).error(function (reason) {
                        deferred.reject(reason);
                    });
                    return deferred.promise;
                },
                fbConnected: function () {

                },
                loginFb: function (fbToken) {
                    var deferred = $q.defer();
                    $http({
                        method: 'POST',
                        url: url,
                        data: "access_token=" + fbToken,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data) {
                        webStorage.local.add('token', data.token);
                        webStorage.local.add('username', data.username);
                        deferred.resolve({
                            success: 'login',
                            username: data.username
                        });
                    }).error(function (reason) {
                        deferred.reject(reason);
                    });
                    return deferred.promise;
                },
                logout: function () {
                    webStorage.local.remove('token');
                    webStorage.local.remove('username');
                },
                user: function () {
                    return {
                        username: webStorage.local.get('username'),
                        token: webStorage.local.get('token')
                    };
                }
            };
        }
    ];
});