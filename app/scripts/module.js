/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */

var dropspot = angular.module('dropspot',
        ['ui.router',
            'services.config',
            'ui.bootstrap',
            'djangoRESTResources',
            'AngularGM',
            'ngSanitize',
            'http-auth-interceptor',
            'ngAnimate',
            'dropspot.tokenauth',
            'ngAutocomplete',
            'angularFileUpload',
            'ngClipboard',
            'infinite-scroll',
            'pickadate',
            'facebook'])
.run(['$rootScope', '$state', '$stateParams',
     function ($rootScope, $state, $stateParams) {
         // It's very handy to add references to $state and $stateParams to the $rootScope
         // so that you can access them from any scope within your applications.For example,
         // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
         // to active whenever 'contacts.list' or one of its decendents is active.
         $rootScope.$state = $state;
         $rootScope.$stateParams = $stateParams;
     }]);

// Collection constructor function to encapsulate HTTP and pagination logic
dropspot.factory('Collection', ['$http', 'configuration', function($http, configuration) {
  var Collection = function() {
    this.items = [];
    this.busy = false;
    this.page = '';
    this.after = 1
  };

  Collection.prototype.nextPage = function(path) {
    if (this.busy) return;
    this.busy = true;

    var url = path + "?page_size=20&page=" + this.after;
    $http.get(url).success(function(data) {
        var results = data['results'];
        for (var i = 0; i < results.length; i++) {
            this.items.push(results[i]);
        }
        if (data['next'] != null){
            this.after += 1;
            this.busy = false;
        }
    }.bind(this));
  };

  Collection.prototype.createCollection = function(name, description){
    var url = configuration.API_URL + "/collections/.json?";
    return $http.post(url,  JSON.stringify(
                { 'name' : this.name, 'description' : this.description }));
  };
  return Collection;
}]);

// Subscription constructor function to encapsulate HTTP and pagination logic
dropspot.factory('Subscription', ['$http', function($http) {
  var Subscription = function() {
    this.items = [];
    this.busy = false;
    this.page = '';
    this.after = 1;
  };

  Subscription.prototype.nextPage = function(path) {
    if (this.busy) return;
    this.busy = true;

    var url = path + "?page_size=20&page=" + this.after;
    $http.get(url).success(function(data) {
        var results = data['results'];
        for (var i = 0; i < results.length; i++) {
            this.items.push(results[i]['content_object']);
        }
        if (data['next'] != null){
            this.after += 1;
            this.busy = false;
        }
    }.bind(this));
  };

  return Subscription;
}]);

dropspot.filter('reverse', function() {
  return function(items) {
    if(typeof items === 'undefined') { return; }
    return angular.isArray(items) ?
      items.slice().reverse() : // If it is an array, split and reverse it
      (items + '').split('').reverse().join(''); // else make it a string (if it isn't already), and reverse it
  };
});
