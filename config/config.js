/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/dashboard/src/master/LICENSE)
 */


'use strict';

angular.module('services.config', [])
  .constant('configuration', {
    API_URL: '@@API_URL',
    FB_APP_ID: '@@FB_APP_ID'
  });
